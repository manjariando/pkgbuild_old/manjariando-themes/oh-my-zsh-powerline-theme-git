# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor:  jyantis <yantis@yantis.net>

_pkgname=oh-my-zsh-powerline-theme
pkgname=${_pkgname}-git
pkgver=r81
zshver=5.8
pkgrel=6
pkgdesc='oh-my-zsh Powerline style theme'
arch=('any')
url='https://github.com/jeremyFreeAgent/oh-my-zsh-powerline-theme'
license=('custom')
makedepends=('git' "zsh=${zshver}")
provides=("${pkgname%%-git}")
conflicts=("${pkgname%%-git}" 'bullet-train-oh-my-zsh-theme' 'bullet-train-oh-my-zsh-theme-git')
install=${pkgname%%-git}.install
source=('git+https://github.com/jeremyFreeAgent/oh-my-zsh-powerline-theme.git'
        "https://metainfo.manjariando.com.br/${_pkgname}/com.oh-my-zsh-powerline.metainfo.xml"
        "https://metainfo.manjariando.com.br/${_pkgname}/oh-my-zsh-theme"-{48,64,128}.png)
sha256sums=('SKIP'
            'dff0a1f2de71c99fe077433d088a74a59ced3a28a3cb20d9f8775a12a1b063bc'
            'e59d45e84180304b483f4f70dccb5577fcd35dd0b96a336eeaf8d720e9ef8a12'
            '610fa5c143c472fdd7e35e67f98fe01a66bd9d85266754bae9ff148790c5fecb'
            '17d3c28a3a73711d734fcccb5a43e5d15b98ca922c21cd7164739c718506c9f6')

pkgver() {
    cd ${pkgname%%-git}
    printf "r%s" "$(git rev-list --count HEAD)"
}

_zsh_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_zsh_desktop" | tee com.oh_my_zsh_powerline.desktop
}

package() {
    depends=('zsh' 'oh-my-zsh')

    cd ${pkgname%%-git}

    # We don't need anything related to git in the package
    rm -rf .git*

    # No license
    # Install Readme as License in case in the future some type of license is added
    install -D -m644 README.md "${pkgdir}/usr/share/licenses/${pkgname%%-git}/LICENSE"

    # Install Documentation
    install -D -m644 README.md "${pkgdir}/usr/share/doc/${pkgname%%-git}/README.md"

    # Install the theme
    install -D -m644 powerline.zsh-theme "${pkgdir}/usr/share/oh-my-zsh/themes/powerline.zsh-theme"

    # Appstream
    install -Dm644 "${srcdir}/com.oh-my-zsh-powerline.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.oh-my-zsh-powerline.metainfo.xml"
    install -Dm644 "${srcdir}/com.oh_my_zsh_powerline.desktop" \
      "${pkgdir}/usr/share/applications/com.oh_my_zsh_powerline.desktop"

    for i in 48 64 128; do
        install -Dm644 "${srcdir}/oh-my-zsh-theme-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/oh-my-zsh-powerline.png"
    done

    # fix .install file
    sed -i "s:ZSH=.*:ZSH=${zshver}:" "${startdir}/${pkgname%%-git}.install"
}
